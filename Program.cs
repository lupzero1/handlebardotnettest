﻿using System;
using System.IO;
using HandlebarsDotNet;

namespace TestR
{
    class Program
    {
        static void Main(string[] args)
        {

            string source = File.ReadAllText("emailsummary.html");
            var template = Handlebars.Compile(source);

            // var data = new
            // {
            //     user = [{ name: "jone", userId: 1}]
            // };
            var data = new
            {
                news = new[]{
                    new {
                        title = "Topic 1", body = "body 1"
                    },
                    new {
                        title = "Topic 2", body = "body 2"
                    }
                }
            };


            var result = template(data);

            Console.WriteLine(result);
        }
    }
}
